  ##  Read any item that does
  ##  not require Super User
  ##  privileges.

path "*" {
    capabilities = ["read"]
}
