  ##  Delete any item that does not 
  ##  require Super User privileges.

path "*" {
    capabilities = ["delete"]
}
