# Vault Replication Setup

Sets up Vault Enterprise Disaster Recovery (DR) Replication between two clusters, as a "DR Replication Set".

The first Vault Cluster should be the Vault Cluster you intend to use as the DR Primary Cluster.

The second should be the Vault Cluster you intend to use as the DR Secondary Cluster. 

The DR Secondary Cluster will copy data from the DR Primary Cluster.

Make sure you have the `jq` CLI utility installed for your system. 

https://stedolan.github.io/jq/

If you're on Windows, I recommend installing bash and git from here https://git-scm.com/.

## Usage

`replication_setup.sh <primary VAULT_ADDR> <secondary VAULT_ADDR> <primary VAULT_TOKEN> <secondary VAULT_TOKEN>`

## Test Example

### Download Vault Enterprise
 
Place it in the `clusters/` folder, here.

Run `./vault --version` to get the Vault version. It should say `pro` or `prem` in the output of that command.

### Create a Test Vault

Open a new Bash terminal.

`VAULT_UI=true VAULT_REDIRECT_ADDR=http://127.0.0.1:8200 ./vault server -log-level=trace -dev -dev-root-token-id=root -dev-listen-address=127.0.0.1:8200 -dev-ha -dev-transactional`

### Create Another Test Vault

Open a new Bash Terminal.

`VAULT_UI=true VAULT_REDIRECT_ADDR=http://127.0.0.1:8202 ./vault server -log-level=trace -dev -dev-root-token-id=toor -dev-listen-address=127.0.0.1:8202 -dev-ha -dev-transactional`

### Run the Script
 
Open yet another Bash Terminal.

`replication_setup.sh "http://127.0.0.1:8200" "http://127.0.0.1:8202" "root" "toor"`

### Retry

Just enter `ctrl-C` in each of the two terminals in which you ran Vault, then start Vault again the same way. 

This clears all of the data from each Vault.

You can then run the DR script again.
